//
//  ViewController.swift
//  NumberSharer
//
//  Created by James Cash on 14-12-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import NumberSharerCommon

class ViewController: UIViewController {

    @IBOutlet var numberSlider: UISlider!

    override func viewWillAppear(_ animated: Bool) {
        numberSlider.value = UserDefaults(suiteName: sharedSuiteName)?.value(forKey: numberDefaultsKey) as? Float ?? 50
    }

    @IBAction func numberChanged(_ sender: UISlider) {
        UserDefaults(suiteName: sharedSuiteName)?.set(sender.value, forKey: numberDefaultsKey)
    }

}

