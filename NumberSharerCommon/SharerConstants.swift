//
//  SharerConstants.swift
//  NumberSharer
//
//  Created by James Cash on 14-12-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import Foundation


public let sharedSuiteName = "group.occasionallycogent.testing"
public let numberDefaultsKey = "sharedNumberValue"
