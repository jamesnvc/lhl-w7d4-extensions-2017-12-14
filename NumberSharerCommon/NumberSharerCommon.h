//
//  NumberSharerCommon.h
//  NumberSharerCommon
//
//  Created by James Cash on 14-12-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NumberSharerCommon.
FOUNDATION_EXPORT double NumberSharerCommonVersionNumber;

//! Project version string for NumberSharerCommon.
FOUNDATION_EXPORT const unsigned char NumberSharerCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NumberSharerCommon/PublicHeader.h>


