//
//  ShareViewController.swift
//  NumberSharerShare
//
//  Created by James Cash on 14-12-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social
import NumberSharerCommon

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        let fmtr = NumberFormatter()
        fmtr.allowsFloats = true
        guard let num = fmtr.number(from: contentText) else { return false }
        let float = num.floatValue
        return 0 <= float && float <= 100
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
        let fmtr = NumberFormatter()
        fmtr.allowsFloats = true
        let num = fmtr.number(from: contentText)!.floatValue
        UserDefaults(suiteName: sharedSuiteName)?.set(num, forKey: numberDefaultsKey)

        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
