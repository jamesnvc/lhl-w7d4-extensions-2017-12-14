//
//  TodayViewController.swift
//  NumberSharerToday
//
//  Created by James Cash on 14-12-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import NumberSharerCommon

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet var numberStepper: UIStepper!
    @IBOutlet var numberLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSavedNumber()
    }
    
    func loadSavedNumber() {
        let number = UserDefaults(suiteName: sharedSuiteName)?.value(forKey: numberDefaultsKey) as? Float ?? 50
        numberStepper.value = Double(number)
        numberLabel.text = "\(number)"
    }

    @IBAction func stepNumber(_ sender: UISlider) {
        UserDefaults(suiteName: sharedSuiteName)?.set(Float(sender.value), forKey: numberDefaultsKey)
        numberLabel.text = "\(sender.value)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        loadSavedNumber()
        completionHandler(NCUpdateResult.newData)
    }
    
}
